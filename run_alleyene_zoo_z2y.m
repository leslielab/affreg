%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Rafi Pelossof, MSKCC, 2015
%
%       generate figure S24-S27 array reconstruction from zscores *** requires at least 24GB ram ***
%

function run_alleyene_zoo_z2y
fprintf('This function requires 24GB of free RAM and a strong processor');
%waitforbuttonpress;

source = 'zoo218';

zoo_counts = importdata('data/zoo_counts.csv');
zoo_zscores = importdata('data/zoo_y_nodup.csv');
P_zoo = zoo_counts.data;
y_zoo = zoo_zscores.data;
seqs = importdata('data/zoo_seqs_218.fa');
names_zoo = seqs(1:2:end);
seqs_zoo  = seqs(2:2:end);
expfactor = 1.5;

% load alleyene data for training
data = load('data/alleyene_data.mat');
prot_counts = importdata('data/alleyene_prot_counts.csv');
seqs_al = importdata('data/alleyene_prots_seqs.fa');
names_al = seqs_al(1:2:end);
seqs_al  = seqs_al(2:2:end);

P = double(prot_counts.data>0);
ix = sum(P)>3;
P = P(:, ix);
P_zoo = double(P_zoo(:, ix) > 0);
D = double(data.D>0);
ix = sum(D)>3;
D = D(:, ix);

Y_al = quantilenorm(zscore(data.Y));
Y_algr = data.Y;
Y_zoo = y_zoo;

isexpy = 0;
is_centered = 1;

if isexpy
    Y_al = expfactor.^(Y_al);
end

if is_centered
    mu = mean(Y_al,2);
else
    mu = zeros(size(Y_al, 1), 1);
end
Y_al = Y_al - repmat(mu, 1, size(Y_al,2));


n = size(Y_al,2); %number of proteins

lambdas = [1];
rsL2 = 0;
spectrumA = [1];
spectrumB = [1];

lambda = lambdas;
fprintf('starting %f\n', lambda);
model = ar_train(D, P, Y_al, lambda, rsL2, spectrumA, spectrumB);
preds = ar_predict(D, P, Y_al, model);
zoo_preds = ar_predict(D, P_zoo, Y_al, model);

idx_nn = knnsearch(P, P_zoo);
nn_rec = Y_algr(:,idx_nn);
idx_or = knnsearch(Y_algr', Y_zoo');
or_rec = Y_algr(:,idx_or); 
similarity = nwalign(seqs_zoo, seqs_al);
[~, idx] = max(similarity,[],2);
bl_rec = Y_algr(:,idx);

ar_rec = zoo_preds.rec + repmat(mu, 1, length(idx_nn));

clear D
clear data
clear zoo_zscores


% load array data
zooY1 = csvread('data/zoo_intensities_Y1.csv',1,1);
zooY2 = csvread('data/zoo_intensities_Y2.csv',1,1);
zooD1 = csvread('data/zoo_intensities_D1.csv',1,1);
zooD2 = csvread('data/zoo_intensities_D2.csv',1,1);
zooY1q = quantilenorm(zooY1);
zooY1qn = bsxfun(@times, zooY1q, 1./sqrt(sum(zooY1q.^2)));
zooY2q = quantilenorm(zooY2);
zooY2qn = bsxfun(@times, zooY2q, 1./sqrt(sum(zooY2q.^2)));
zooZ = quantilenorm(zscore(Y_zoo));


% 1. reconstruct through pinv(D), a 6-mer model
zooZ1 = zooD1'*zooY1qn;
zooZ2 = zooD2'*zooY2qn;
h=figure;
plot(diag(corr(zooZ1, zooZ2,'type','spearman')));
title('correlation between zscore models for arr1 and 2');
xlabel('TF index'); ylabel('zscore correlation arr1 and arr2');
print(h, 'results/alleyene_zoo_z2y_zreproduce.pdf','-dpdf');

h=figure;
plot(zooZ1(:,1), zooZ2(:,1),'o')
xlabel('zscore array 1'); ylabel('zscore array 2'); title('Zscore reproducibility for 6-mers for Zoo TF1')
text(2.5, 6, sprintf('R^2=%.2f', corr(zooZ1(:,1), zooZ2(:,1),'type','spearman')^2))
print(h, 'results/alleyene_zoo_z2y_zreproduce1.pdf','-dpdf');

pinvD1 = pinv(zooD1);
pinvD2 = pinv(zooD2);
ypredd11 = pinvD1'*zooZ1;
ypredd21 = pinvD1'*zooZ2;
ypredd12 = pinvD2'*zooZ1;
ypredd22 = pinvD2'*zooZ2;

n = size(zooY1qn, 2);
n_folds = 10;
fold_ix = round(linspace(0,n, n_folds+1));
ypredq11 = zeros(size(zooY1qn));
ypredq12 = zeros(size(zooY2qn));
ypredq22 = zeros(size(zooY2qn));
ypredq21 = zeros(size(zooY1qn));
lambda = 10;
for fold = 1:n_folds
    fprintf('%d\n', fold);
    % create subsets
    test_ix = fold_ix(fold)+1:fold_ix(fold+1);
    train_ix = setdiff(1:n, test_ix);
    %Q_from_to
    Q11 = inv(zooZ1(:,train_ix)*zooZ1(:,train_ix)' + lambda*eye(size(zooZ1(:,train_ix), 1)))*zooZ1(:,train_ix)*zooY1qn(:,train_ix)';
    Q22 = inv(zooZ2(:,train_ix)*zooZ2(:,train_ix)' + lambda*eye(size(zooZ2(:,train_ix), 1)))*zooZ2(:,train_ix)*zooY2qn(:,train_ix)';
    Q12 = inv(zooZ1(:,train_ix)*zooZ1(:,train_ix)' + lambda*eye(size(zooZ1(:,train_ix), 1)))*zooZ1(:,train_ix)*zooY2qn(:,train_ix)';
    Q21 = inv(zooZ2(:,train_ix)*zooZ2(:,train_ix)' + lambda*eye(size(zooZ2(:,train_ix), 1)))*zooZ2(:,train_ix)*zooY1qn(:,train_ix)';
    ypredq11(:, test_ix) = Q11'*zooZ1(:, test_ix);
    ypredq22(:, test_ix) = Q22'*zooZ2(:, test_ix);
    ypredq12(:, test_ix) = Q12'*zooZ1(:, test_ix);
    ypredq21(:, test_ix) = Q21'*zooZ2(:, test_ix);
end
h=figure;
subplot(2, 2, 1)
plot(diag(corr(ypredd11, zooY1qn)), diag(corr(ypredq11, zooY1qn)), 'o',...
    'markerfacecolor', [55,126,184]/255, 'markeredgecolor', 'none', 'markersize',5)
line([0 1],[0 1])
xlabel('pinv(D_1)''Z_1 reconstruction correlation'); ylabel('Q11 reconstruction correlation'); 
title('Reconstruction of Y1 from Z1')
subplot(2, 2, 2)
plot(diag(corr(ypredd21, zooY1qn)), diag(corr(ypredq21, zooY1qn)), 'o',...
    'markerfacecolor', [55,126,184]/255, 'markeredgecolor', 'none', 'markersize',5)
line([0 1],[0 1])
xlabel('pinv(D_1)''Z_2 reconstruction correlation'); ylabel('Q21 reconstruction correlation'); 
title('Reconstruction of Y1 from Z2')
subplot(2, 2, 3)
plot(diag(corr(ypredd12, zooY2qn)), diag(corr(ypredq12, zooY2qn)), 'o',...
    'markerfacecolor', [55,126,184]/255, 'markeredgecolor', 'none', 'markersize',5)
line([0 1],[0 1])
xlabel('pinv(D_2)''Z_1 reconstruction correlation'); ylabel('Q12 reconstruction correlation'); 
title('Reconstruction of Y2 from Z1')
subplot(2, 2, 4)
plot(diag(corr(ypredd22, zooY2qn)), diag(corr(ypredq22, zooY2qn)), 'o',...
    'markerfacecolor', [55,126,184]/255, 'markeredgecolor', 'none', 'markersize',5)
line([0 1],[0 1])
xlabel('pinv(D_2)''Z_2 reconstruction correlation'); ylabel('Q22 reconstruction correlation'); 
title('Reconstruction of Y2 from Z2')
print(h, 'results/zoo_z2y_reconstructions.pdf','-dpdf');


% 2. rectonstruct Zoo arrays from PBM zscores
n = size(zooZ, 2);
n_folds = 10;
fold_ix = round(linspace(0,n, n_folds+1));
ypred1n = zeros(size(zooY1qn));
ypred2n = zeros(size(zooY2qn));
for fold = 1:n_folds
    fprintf('%d\n', fold);

    % create subsets
    test_ix = fold_ix(fold)+1:fold_ix(fold+1);
    train_ix = setdiff(1:n, test_ix);

    z = zooZ(:, train_ix);
    Q = inv(z*z' + 500*eye(size(z, 1)))'*z;

    ypred1n(:,test_ix) = zooY1qn(:, train_ix)*Q'*(zoo_preds.rec(:, test_ix)+repmat(mu, 1, length(test_ix)));
    ypred2n(:,test_ix) = zooY2qn(:, train_ix)*Q'*(zoo_preds.rec(:, test_ix)+repmat(mu, 1, length(test_ix)));
end
%cy1 - correlation of y prediction from array 1 to array 1
%cz1 - correlation of z score prediction on array 1
for i=1:size(ypred1n, 2); cy1(i) = corr(zooY1qn(:,i), ypred1n(:,i)      , 'type', 'spearman'); end;
for i=1:size(ypred1n, 2); cz1(i) = corr(Y_zoo(:,i), zoo_preds.rec(:,i)+mu, 'type', 'spearman'); end;
save('results/alleyene_zoo_z2y_1.mat', 'cy1','cz1', 'ypred1n');

%cy1 - correlation of y prediction from array 1 to array 2
%cz1 - correlation of z score prediction on array 2
for i=1:size(ypred2n, 2); cy2(i) = corr(zooY2qn(:,i), ypred2n(:,i)      , 'type', 'spearman'); end;
for i=1:size(ypred2n, 2); cz2(i) = corr(Y_zoo(:,i), zoo_preds.rec(:,i)+mu, 'type', 'spearman'); end;
save('results/alleyene_zoo_z2y_1.mat', 'cy2','cz2', 'ypred2n');

h = figure;
plotScatter(cz1(:), cy1(:), 0.008, 1, [55,126,184]/255, ...
     'zscore correlation', 'Probe correlation', 'Comparison of zscore vs array correlations Arr1');
print(h, 'results/alleyene_zoo_z2y_1.pdf','-dpdf');

h = figure;
plotScatter(cz2(:), cy2(:), 0.008, 1, [55,126,184]/255, ...
     'zscore correlation', 'Probe correlation', 'Comparison of zscore vs array correlations Arr2');
print(h, 'results/alleyene_zoo_z2y_2.pdf','-dpdf');

h=figure; imagesc(corr(zooY1qn, zooY1qn));
colorbar; colormap jet;
title('Probe intensity correlation Array 1')
print(h, 'results/alleyene_zoo_arrcorr_1.pdf','-dpdf');
h=figure; imagesc(corr(zooY2qn, zooY2qn));
colorbar; colormap jet;
title('Probe intensity correlation Array 2')
print(h, 'results/alleyene_zoo_arrcorr_2.pdf','-dpdf');

% r2 of the correlation between out prediction quality and the array
% reproducibily per protein
%r2_1 = corr(cy1', mean(corr(zooY1qn, zooY1qn))')^2
%r2_2 = corr(cy2', mean(corr(zooY2qn, zooY2qn))')^2

h = figure;
rep = mean(corr(zooY1qn, zooY1qn))';
plotScatter(cy1(:), rep(:), 0.008, 1, [55,126,184]/255, ...
     'Prediction correlation', 'Average actual probe correlation', 'Comparison of prediction and probe correlation, Arr1',0,0);
print(h, 'results/alleyene_zoo_predarrcorr_1.pdf','-dpdf');

h = figure;
rep = mean(corr(zooY2qn, zooY2qn))';
plotScatter(cy2(:), rep(:), 0.008, 1, [55,126,184]/255, ...
     'Prediction correlation', 'Average actual probe correlation', 'Comparison of prediction and probe correlation, Arr2',0,0);
print(h, 'results/alleyene_zoo_predarrcorr_2.pdf','-dpdf');



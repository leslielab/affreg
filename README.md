README


AffinityRegression explains interaction data as an interaction between two given representations. 
The code will simulate such interaction data, and demonstrate the recovery of the model as well as its performance.
simulate.m will run a simulation where the data (D,W,P) is generated from a gaussian with zero mean, and unit variance (you can set n,p,m,q to change dimensionality). After sampling W and constructing Y = DWP', the simulation will try to solve for W the given D,P,Y. The solution will be reconstructed from the compressed \tilde W learned with the output kernel Y'Y and svd compression.


You need to install SLEP to run the code: http://www.public.asu.edu/~jye02/Software/SLEP/

Extensive code together with data can be found at:
http://cbio.mskcc.org/~pelossof/ar_run.zip


Rafi Pelossof MSKCC 2014
%ph Jan 2011
% 
% Counts the presence of kmers in PBM seqs
%
%   Support files for matlab based DNA kmer counting
%   By Phaedra Agius
%
% Input: k=kmer length
%        seqs=sequences
%        A=alphabet ('nt' or 'aa')
%        rev=1 for revcomp kmers, 0 otherwise (this only for nt alphabet)
%
% Output: F=kmer counts in seqs 
%         kmers=kmers counted (kmers with counts of 0 are removed)
%
% function [F,kmers]=count_kmers(k,seqs,A,rev)

function [F,kmers]=count_kmers(k,seqs,A,rev)

%finding all possible kmers
if strcmpi(A,'nt')==1
    A='ACGT';
elseif strcmpi(A,'aa')==1
    A='ACDEFGHIKLMNPQRSTVWY';
else
    error('A must be nt or aa')
end
x=nchoosek(repmat(1:length(A),1,k),k);
u=unique(cellstr(A(x))); kmers=u;

%counting all kmers in the seqs
s=char(seqs);
Q=1-ismember(s,A);  % =1 where -,* or other special markers are used
F=zeros(size(seqs,1),length(u));
for i=1:size(s,2)-k+1
    tmp=cellstr(s(:,i:i+k-1));
    q=find(sum(Q(:,i:i+k-1),2)>0);
    tmp(q,:)=[];   % remove columns which do not contain specific acids
    I=(1:size(F,1))'; I(q)=[];
    [~,~,J]=unique([tmp; u]);
    J=J(1:length(tmp));
    IDX=sub2ind(size(F),I,J);
    F(IDX)=F(IDX)+1;
end

%for nt kmers
if nargin>3 
    if rev>0
        %finding kmer revcomps and adjusting counts accordingly
        u=kmers;
        kmers(:,2)=cellstr(revcomps(char(kmers)));
        kmers=unique_kmers(kmers(:,1),kmers(:,2));
        P=zeros(length(seqs),length(kmers));
        [~,I,J]=intersect(u,kmers(:,1));
        P(:,J)=F(:,I);
        [~,I,J]=intersect(u,kmers(:,2));
        P(:,J)=P(:,J)+F(:,I);
        %adjusting counts for palindromes
        q=char(kmers(:,1))*1-char(kmers(:,2))*1;
        q=sum(abs(q),2);
        q=find(q==0);
        P(:,q)=P(:,q)/2;
        F=P;
    end
end    

q=find(sum(F)==0);
if length(q)>0
    F(:,q)=[];
    kmers(q,:)=[];
end







%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%ph May 2009
%
% Input: u = set of sequences 
%
% Output: ur = set of revcomps
%
%function ur=revcomps(u)

function ur=revcomps(u)

ur=char(u);
ka=ismember(ur,'A');  
kc=ismember(ur,'C');  
kg=ismember(ur,'G');  
kt=ismember(ur,'T');

ur(ka==1)='T'; 
ur(kc==1)='G'; 
ur(kg==1)='C'; 
ur(kt==1)='A';

ur=cellstr(fliplr(ur));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%ph updated June 2009 
%
% Removes repeated kmers in reverse compliments
%
% Input: u= (cell) o kmers, ur= (cell) of kmer reverse compliments
%
% Output: U = 2 dim cell with unique kmers and reverse compliments
%
% function U=unique_kmers(u,ur)

function U=unique_kmers(u,ur)

%palindromes
kk=find(sum(abs(char(u)-char(ur)),2)==0); 

UF=u(kk,:); UR=ur(kk,:); %starting new storage vessels with palindromes
u(kk,:)=[]; ur(kk,:)=[];    %removing the palindromes from u and ur

%finding cases were a seq and its revcomp is present in both u and ur
[dd,I]=intersect(u,ur); %dd=doubles
ddr=ur(I,:); %corresponding revcomps
u(I,:)=[]; ur(I,:)=[]; %removing these from u and ur
UF=[UF; u]; UR=[UR; ur]; %and storing what's left of u and ur in UF and UR

if length(dd)>0
    ddsort=sort(dd); %imposing some order on the repeated elements

    [tmp,I]=intersect(dd,ddsort); %index of dd wrt ddsort
    [tmp,J]=intersect(ddr,ddsort); %index of ddr wrt ddsort

    q=[I J]; %index of ddsort in dd and ddr
    qsort=sort(q')'; %Eg. ddsort=[a a' b b'], I=[1 10 22 25] and J=[10 1 25 22] since dd=[a; a'; b; b'] and ddr=[a'; a; b'; b]
    qnew=unique(qsort,'rows'); %w/ref to eg .. qnew=unique([1 10; 1 10; 22 25; 22 25])
    dd=dd(qnew(:,1),:);
    ddr=ddr(qnew(:,1),:);

    UF=[UF; dd];
    UR=[UR; ddr];
end

U=[cellstr(UF) cellstr(UR)]; %storing as a cell